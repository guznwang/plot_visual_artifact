import plotly.graph_objects as go
import pandas as pd

data=pd.read_csv('iris.csv')  #获取数据，csv文件也已上传
index=list(data.groupby('Species').count().index) #获取鸢尾花的种类
color_num=range(3)
e=dict(zip(index,color_num)) #打包成字典
data['color']=data['Species'].map(e)  #新加一列用于颜色的分类，将鸢尾花的种类划分
dot=go.Scatter(x=data['Sepal.Length'],y=data['Sepal.Width'],
               mode='markers',marker={'color':data['color']},) #mode设置成散点的样式
fig=go.Figure(dot)
fig.show()