import plotly.graph_objects as go
import tushare as ts

stock = ts.get_k_data('000425', autype='hfq',  ktype='d', start='2013-1-1', end='2017-12-31') #获取数据
line1=go.Scatter(x=stock['date'],y=stock['high'],name='high')
line2=go.Scatter(x=stock['date'],y=stock['low'],name='low')
line3=go.Scatter(x=stock['date'],y=stock['high']-stock['low'],name='差值')
fig=go.Figure([line1,line2,line3])
fig.update_layout(title='散点图',xaxis_title="data",yaxis_title='数据值') #添加一些说明
fig.show()

