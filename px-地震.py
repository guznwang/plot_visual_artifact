import plotly.graph_objects as go
import pandas as pd

data=pd.read_csv('earthquakes.csv')
my_map=go.Densitymapbox(lat=data['Latitude'],lon=data['Longitude'],z=data['Magnitude'],radius=6) #rafius为点的大小，可更改
fig=go.Figure(my_map)
fig.update_layout(mapbox_style="stamen-terrain") #设置地图样式，没有此行会是空白
fig.show()