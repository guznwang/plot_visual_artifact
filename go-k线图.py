import plotly.graph_objects as go
import tushare as ts

data=ts.get_k_data('000425', autype='hfq',  ktype='d', start='2013-1-1', end='2017-12-31') #获取数据
data=data[(data['date']>='2013-10-08')&(data['date']<='2013-10-18')]
line=go.Candlestick(x=data['date'],open=data['open'],high=data['high'],low=data['low'],close=data['close'])
fig=go.Figure([line])
fig.show()
