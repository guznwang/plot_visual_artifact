import plotly.graph_objects as go
import tushare as ts

stock = ts.get_k_data('000425', autype='hfq',  ktype='d', start='2013-1-1', end='2017-12-31') #获取数据
stock=stock[(stock['date']>="2013-10-08")&(stock['date']<="2013-10-18")]   #数据取2013-10-08至2013-10-18
line1=go.Bar(x=stock['date'],y=stock['high'],name='high',text=stock['high'],textposition='outside') #绘制直方图，x轴为日期，y轴为最高点，设置在直方图上方显示数据文本
line2=go.Bar(x=stock['date'],y=stock['low'],name='low',text=stock['low'],textposition='outside')
fig=go.Figure([line1,line2])  #设置画布
fig.update_layout(title='直方图',xaxis_title="data",yaxis_title='数据')  #添加一些说明
fig.show()

